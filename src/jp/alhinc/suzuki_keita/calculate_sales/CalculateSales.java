package jp.alhinc.suzuki_keita.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;




public class CalculateSales {
	static HashMap<String, String> mapBranch = new HashMap<String, String>(); //支店mapの生成
	static HashMap<String,Long> mapCulculate = new HashMap<String,Long>(); //売り上げ集計mapの作成

	public static boolean inFile(String filePath, String fileName, String fileFormat, String fileType, HashMap<String, String> nameMap, HashMap<String, Long> detailMap){
		BufferedReader br = null;
		try {
			File file = new File(filePath, fileName);
			if(! file.exists()) {
				System.out.println(fileType + "ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String selectFile;

			while((selectFile = br.readLine()) !=null) {
				if(! (selectFile.matches(fileFormat))) {
					System.out.println(fileType + "ファイルのフォーマットが不正です");
					return false;
				}

				String[] lineSplit = selectFile.split(",");  //コードと支店名に分割

				if(lineSplit.length != 2){
					System.out.println(fileType + "ファイルのフォーマットが不正です");
					return false;
				}

				nameMap.put(lineSplit[0],lineSplit[1]);
				detailMap.put(lineSplit[0], 0L );
			}

		}
		catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally {
			if(br != null){
				try {
					br.close();
				}
				catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}


	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		// 支店ファイルの入力
		if(! inFile(args[0],"branch.lst", "[0-9]{3},.+", "支店定義", mapBranch, mapCulculate)) {
			return;
		}

		//売り上げデータの入力
		BufferedReader brSales = null;
		ArrayList<File> fileSelection = new ArrayList<File>(); //選別用リスト
		File fileSales = new File(args[0]);
		File[] listSales = fileSales.listFiles();

		//ファイルの選別
		for(File salesSurvey : listSales) {
			if(salesSurvey.getName().matches("[0-9]{8}.rcd")) {
				if(salesSurvey.isFile()) {
					fileSelection.add(salesSurvey);
				}
			}
		}

		//欠番エラーチェック
		for(int i = 0; i < fileSelection.size() -1; i++ ) {
			String[] numberSplit = fileSelection.get(i).getName().split("\\.");
			int fileNumber = Integer.parseInt(numberSplit[0]);

			String[] nextNumberSplit = fileSelection.get(i+1).getName().split("\\.");
			int nextFileNumber = Integer.parseInt(nextNumberSplit[0]);

			if(fileNumber +1 != nextFileNumber) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//集計
		for(int i = 0; i < fileSelection.size(); i++) {
			ArrayList<String> culculateList = new ArrayList<String>(); //[0][1]のみを使うためにここに生成

			try {
				FileReader frSales = new FileReader(fileSelection.get(i));
				brSales = new BufferedReader(frSales);
				String lineSales;

				while((lineSales = brSales.readLine()) != null) {
					//売り上げファイルを読み込み、集計リストに格納（0,1のみ）
					culculateList.add(lineSales);
				}

				//行数エラーチェック
				if(culculateList.size() != 2){
					System.out.println(fileSelection.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				//支店コードのエラーチェック
				if(! mapCulculate.containsKey(culculateList.get(0))) {
					System.out.println(fileSelection.get(i).getName() + "の支店コードが不正です");
					return;
				}

				//売り上げ金額エラーチェック
				if(! culculateList.get(1).matches("^[0-9]+$")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//keyが同じものに対して、売り上げを足していく
				Long  summary = mapCulculate.get(culculateList.get(0)) ;
				Long adding = Long.parseLong(culculateList.get(1));
				summary += adding ;

				//10桁チェック
				if(summary > 9999999999L ) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				mapCulculate.put(culculateList.get(0),summary );
			}

			catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}

			finally {
				if(brSales != null) {
					try {
						brSales.close();
					}
					catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//ファイルの出力
		if(! outFile(args[0], "branch.out", mapBranch, mapCulculate)) {
			return;
		}
	}

	public static boolean outFile(String fileOutPath, String fileName, HashMap<String,String> nameMap,HashMap<String,Long> detailMap){
		BufferedWriter bwOut = null;
		try {
			File outFile = new File(fileOutPath , fileName);
			FileWriter fwOut = new FileWriter(outFile);
			bwOut = new BufferedWriter(fwOut);
			String lineSeparate = System.getProperty("line.separator");
			for(String outKey : nameMap.keySet()) {
				bwOut.write(outKey + "," + nameMap.get(outKey) + "," + detailMap.get(outKey) + lineSeparate);
			}
		}
		catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally{
			if(bwOut != null) {
				try {
					bwOut.close();
				}
				catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
